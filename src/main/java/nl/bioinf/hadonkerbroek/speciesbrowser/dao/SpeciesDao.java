package nl.bioinf.hadonkerbroek.speciesbrowser.dao;


import nl.bioinf.hadonkerbroek.speciesbrowser.model.Role;
import nl.bioinf.hadonkerbroek.speciesbrowser.model.User;

public interface SpeciesDao {

    /**
     * connection logic should be put here
     * @throws DatabaseException
     */
    void connect() throws DatabaseException;

    /**
     * shutdown logic should be put here
     * @throws DatabaseException
     */
    void disconnect() throws DatabaseException;
    /**
     * fetches a user by username and password.
     * @param userName
     * @param userPass
     * @return
     * @throws DatabaseException
     */
    User getUser(String userName, String userPass) throws DatabaseException;

    /**
     * inserts a new User.
     * @param userName
     * @param userPass
     * @param email
     * @param role
     * @throws DatabaseException
     */
    void insertUser(String userName, String userPass, String email, Role role) throws DatabaseException;
}