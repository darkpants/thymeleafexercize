package nl.bioinf.hadonkerbroek.speciesbrowser.dao;

import nl.bioinf.hadonkerbroek.speciesbrowser.model.Role;
import nl.bioinf.hadonkerbroek.speciesbrowser.model.User;

public class SpeciesDaoInMemory implements SpeciesDao {
    @Override
    public void connect() throws DatabaseException {

    }

    @Override
    public void disconnect() throws DatabaseException {

    }

    @Override
    public User getUser(String userName, String userPass) throws DatabaseException {
        return null;
    }

    @Override
    public void insertUser(String userName, String userPass, String email, Role role) throws DatabaseException {

    }
}
