package nl.bioinf.hadonkerbroek.speciesbrowser.model;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class HistoryManager {
    private static final int DEFAULT_MAXIMUM_SIZE = 5;
    private int maximumSize;
    private LinkedList<String> history = new LinkedList<>();

    public HistoryManager() {
        this.maximumSize = DEFAULT_MAXIMUM_SIZE;
    }

    public HistoryManager(int maximumSize) {
        this.maximumSize = maximumSize;
    }

    public void addItem(String historyItem) {
        if (history.size() == maximumSize) {
            history.remove(history.size() -1);
        }
        history.add(0, historyItem);
    }

    public List<String> getHistoryItems() {
        return Collections.unmodifiableList(this.history);
    }

    @Override
    public String toString() {
        return history.toString();
    }
}