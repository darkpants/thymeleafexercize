package nl.bioinf.hadonkerbroek.speciesbrowser.model;

public enum Role {
    GUEST,
    USER,
    ADMIN;
}
