package nl.bioinf.hadonkerbroek.speciesbrowser.model;

public class PantsInfo {
    private String colour;
    private String englishName;
    private String dutchName;
    private String picLoc;

    public PantsInfo() {}

    public PantsInfo(String colour, String englishName, String dutchName, String picLoc) {
        this.colour = colour;
        this.englishName = englishName;
        this.dutchName = dutchName;
        this.picLoc = picLoc;
    }

    public String getColour() {
        return colour;
    }

    public String getEnglishName() {
        return englishName;
    }

    public String getDutchName() {
        return dutchName;
    }

    public String getPicLoc() {
        return picLoc;
    }
}
