package nl.bioinf.hadonkerbroek.speciesbrowser.model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class FileParser {
    public List<PantsInfo> species = new ArrayList<>();

    public PantsList parseFile(String path) {
        try {
            BufferedReader br = new BufferedReader(new FileReader(path));
            String line;
            while ((line = br.readLine()) != null) {
                if (line.startsWith("English_name")) continue;
                String englishName = line.split(",")[0];
                String dutchName = line.split(",")[1];
                String colour = line.split(",")[2];
                String picLoc = line.split(",")[3];
                PantsInfo pantsInfo = new PantsInfo(colour, englishName, dutchName, picLoc);
                species.add(pantsInfo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new PantsList(species);
    }
}
