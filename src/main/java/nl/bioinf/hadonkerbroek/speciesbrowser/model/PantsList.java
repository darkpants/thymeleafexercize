package nl.bioinf.hadonkerbroek.speciesbrowser.model;

import java.util.List;

public class PantsList {
    private static PantsInfo jeans;
    private static PantsInfo sweatpants;
    private static PantsInfo chino;
    private static PantsInfo skipants;
    private static PantsInfo darkpants;
    private static List<PantsInfo> pants;

    public PantsList() {}

    public PantsList(List<PantsInfo> info) {
        pants = info;

        for (PantsInfo pantsInfo : info) {
            switch (pantsInfo.getEnglishName()) {
                case "Jeans":
                    jeans = pantsInfo;
                    break;
                case "Sweatpants":
                    sweatpants = pantsInfo;
                    break;
                case "Chino":
                    chino = pantsInfo;
                    break;
                case "Ski pants":
                    skipants = pantsInfo;
                    break;
                case "Darkpants":
                    darkpants = pantsInfo;
                    break;
            }
        }
    }

    public static PantsInfo getJeans() {
        return jeans;
    }

    public static List<PantsInfo> getPants() {
        return pants;
    }

    public static PantsInfo getChino() {
        return chino;
    }

    public static PantsInfo getDarkpants() {
        return darkpants;
    }

    public static PantsInfo getSkipants() {
        return skipants;
    }

    public static PantsInfo getSweatpants() {
        return sweatpants;
    }
}
