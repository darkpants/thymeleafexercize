package nl.bioinf.hadonkerbroek.speciesbrowser.servlets;


import nl.bioinf.hadonkerbroek.speciesbrowser.config.WebConfig;
import nl.bioinf.hadonkerbroek.speciesbrowser.dao.DatabaseException;
import nl.bioinf.hadonkerbroek.speciesbrowser.dao.SpeciesDao;
import nl.bioinf.hadonkerbroek.speciesbrowser.dao.SpeciesDaoFactory;
import nl.bioinf.hadonkerbroek.speciesbrowser.model.HistoryManager;
import nl.bioinf.hadonkerbroek.speciesbrowser.model.PantsList;
import nl.bioinf.hadonkerbroek.speciesbrowser.model.Role;
import nl.bioinf.hadonkerbroek.speciesbrowser.model.User;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "LoginServlet", urlPatterns = "/login")
public class LoginServlet extends HttpServlet {
    private TemplateEngine templateEngine;

    @Override
    public void init() throws ServletException {
        //no super.init() required
        System.out.println("[LoginServlet] Running no-arg init()");
        this.templateEngine = WebConfig.getTemplateEngine();
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config); //required in this implementation
        System.out.println("[LoginServlet] Running init(ServletConfig config)");
    }

    @Override
    public void destroy() {
        super.destroy();
        System.out.println("[LoginServlet] Shutting down servlet service");
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());

        //fetch the session object
        //if it is not present, one will be created
        HttpSession session = request.getSession();
        String nextPage;

        if (session.getAttribute("user") == null) {
            User user = authenticate(username, password);
            if (user != null) {
                session.setAttribute("user", new User(user.getName(), user.getEmail(), user.getRole()));
                HistoryManager history = new HistoryManager();
                history.addItem("login");
                session.setAttribute("his", history);
                nextPage = "login";
                response.sendRedirect("/home");
            } else {
                ctx.setVariable("message", "Your password and/or username are incorrect; please try again");
                ctx.setVariable("message_type", "error");
                nextPage = "login";
            }
        } else {
            HistoryManager his = (HistoryManager) session.getAttribute("his");
            his.addItem("login");
            session.setAttribute("his", his);
            nextPage = "listing";
            response.sendRedirect("/home");
        }
        templateEngine.process(nextPage, ctx, response.getWriter());
    }

    private User authenticate(String username, String password) {
        final SpeciesDao dataSource = SpeciesDaoFactory.getDataSource();
        try {
            return dataSource.getUser(username, password);
        } catch (DatabaseException e) {
            e.printStackTrace();
        }
        return null;
    }

    //simple GET requests are immediately forwarded to the login page
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Object logout = request.getParameter("logout");

        if (logout != null) {
            session.removeAttribute("user");
            session.removeAttribute("his");
//            response.sendRedirect("/login");
        }

        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());
        ctx.setVariable("message", "Fill out the login form");
        ctx.setVariable("message_type", "info");
        templateEngine.process("login", ctx, response.getWriter());
    }
}