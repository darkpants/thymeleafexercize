package nl.bioinf.hadonkerbroek.speciesbrowser.servlets;


import nl.bioinf.hadonkerbroek.speciesbrowser.config.WebConfig;
import nl.bioinf.hadonkerbroek.speciesbrowser.dao.DatabaseException;
import nl.bioinf.hadonkerbroek.speciesbrowser.dao.SpeciesDao;
import nl.bioinf.hadonkerbroek.speciesbrowser.dao.SpeciesDaoFactory;
import nl.bioinf.hadonkerbroek.speciesbrowser.model.*;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Locale;

@WebServlet(name = "SpeciesServlet", urlPatterns = "/home")
public class SpeciesServlet extends HttpServlet {
    private TemplateEngine templateEngine;

    @Override
    public void init() throws ServletException {
        System.out.println("[SpeciesServlet] Running no-arg init()");
        this.templateEngine = WebConfig.getTemplateEngine();
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config); //required in this implementation
        System.out.println("[SpeciesServlet] Running init(ServletConfig config)");
//        this.path = getServletContext().getInitParameter("data_path");
//        FileParser fileParser = new FileParser();
//        PANTS = fileParser.parseFile(path);
    }

    @Override
    public void destroy() {
        super.destroy();
        System.out.println("[SpeciesServlet] Shutting down servlet service");
    }


    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession();
        HistoryManager history = (HistoryManager) session.getAttribute("history");

        Locale locale = request.getLocale();
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                locale);

        ctx.setVariable("his", history);

        templateEngine.process("listing", ctx, response.getWriter());
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        HttpSession session = request.getSession();
        if (session.getAttribute("user") == null) {
            response.sendRedirect("/login");
        }

        Object user = session.getAttribute("user");

        Locale locale = request.getLocale();
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                locale);

        HistoryManager his = (HistoryManager) session.getAttribute("his");
        his.addItem("home");
        session.setAttribute("his", his);

        ctx.setVariable("user", user);
        ctx.setVariable("allPants", PantsList.getPants());
        ctx.setVariable("his", his);

        templateEngine.process("listing", ctx, response.getWriter());
    }
}
