package nl.bioinf.hadonkerbroek.speciesbrowser.servlets;


import nl.bioinf.hadonkerbroek.speciesbrowser.config.WebConfig;
import nl.bioinf.hadonkerbroek.speciesbrowser.model.HistoryManager;
import nl.bioinf.hadonkerbroek.speciesbrowser.model.PantsList;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Locale;

@WebServlet(name = "SpeciesDetailServlet", urlPatterns = "/pants-details")
public class SpeciesDetailServlet extends HttpServlet {
    private TemplateEngine templateEngine;

    @Override
    public void init() throws ServletException {
        //no super.init() required
        System.out.println("[SpeciesDetailServlet] Running no-arg init()");
        this.templateEngine = WebConfig.getTemplateEngine();
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config); //required in this implementation
        System.out.println("[SpeciesDetailServlet] Running init(ServletConfig config 3)");
    }

    @Override
    public void destroy() {
        super.destroy();
        System.out.println("[SpeciesDetailServlet] Shutting down servlet service");
    }


    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession();
        HistoryManager history = (HistoryManager) session.getAttribute("history");

        Locale locale = request.getLocale();
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                locale);

        ctx.setVariable("his", history);

        templateEngine.process("details", ctx, response.getWriter());
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession();
        if (session.getAttribute("user") == null) {
            response.sendRedirect("/login");
        }

        String pantsName = request.getParameter("pantsName");

        Locale locale = request.getLocale();
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                locale);

        switch (pantsName) {
            case "Darkpants":
                ctx.setVariable("pants", PantsList.getDarkpants());
                ctx.setVariable("pic", PantsList.getDarkpants().getPicLoc());
                break;
            case "Chino":
                ctx.setVariable("pants", PantsList.getChino());
                ctx.setVariable("pic", PantsList.getChino().getPicLoc());
                break;
            case "Jeans":
                ctx.setVariable("pants", PantsList.getJeans());
                ctx.setVariable("pic", PantsList.getJeans().getPicLoc());
                break;
            case "Ski pants":
                ctx.setVariable("pants", PantsList.getSkipants());
                ctx.setVariable("pic", PantsList.getSkipants().getPicLoc());
                break;
            case "Sweatpants":
                ctx.setVariable("pants", PantsList.getSweatpants());
                ctx.setVariable("pic", PantsList.getSweatpants().getPicLoc());
                break;
        }

        HistoryManager his = (HistoryManager) session.getAttribute("his");
        his.addItem(pantsName);
        session.setAttribute("his", his);

        ctx.setVariable("his", his);

        templateEngine.process("details", ctx, response.getWriter());
    }
}
